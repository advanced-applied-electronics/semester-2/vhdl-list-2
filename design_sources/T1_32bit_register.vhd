----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 11/24/2022 10:50:58 AM
-- Design Name: 
-- Module Name: T1_32bit_register - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity T1_32bit_register is
    Port ( WRITE_ENABLE : in STD_LOGIC;
           CLEAR : in STD_LOGIC;
           CLK : in STD_LOGIC;
           D_IN : in STD_LOGIC_VECTOR (31 downto 0);
           D_OUT : out STD_LOGIC_VECTOR (31 downto 0));
end T1_32bit_register;

architecture Behavioral of T1_32bit_register is

begin
d_proc: process(CLK, CLEAR)
begin    

    if CLEAR = '1' then
        D_OUT <= (others => '0');
    
    elsif (rising_edge(CLK) and WRITE_ENABLE = '1') then
        D_OUT <= D_IN;
    else
        
    
    end if;
end process d_proc;


end Behavioral;
