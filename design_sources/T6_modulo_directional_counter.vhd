----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/08/2022 10:01:35 AM
-- Design Name: 
-- Module Name: T6_modulo_directional_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.all;

entity T6_modulo_directional_counter is
    Port ( CLK : in STD_LOGIC;
           RST : in STD_LOGIC;
           DIR : in STD_LOGIC;
           S : in STD_LOGIC;
           MODULO : in STD_LOGIC_VECTOR (5 downto 0);
           CNT_OUT : out STD_LOGIC_VECTOR (5 downto 0));
end T6_modulo_directional_counter;

architecture Behavioral of T6_modulo_directional_counter is

begin
COUNTER_PROC: process(CLK, RST)
variable CNT_MAX: unsigned (5 downto 0);
variable CNT: unsigned (5 downto 0);
begin    

    if RST = '1' then
        CNT_MAX := to_unsigned(1, CNT_MAX'length);
        CNT := (others =>'0');
        CNT_OUT <= (others =>'0');
                
    elsif S = '1' then 
        CNT_MAX := unsigned(MODULO);
        
    elsif rising_edge(CLK) then
        if DIR = '1' then
            
            if (CNT = 0) then 
                CNT := CNT_MAX-1;
            else 
                CNT := (CNT - 1);
            end if;

            CNT := CNT mod to_integer(CNT_MAX);
            CNT_OUT <= std_logic_vector(CNT);
        
        elsif  DIR = '0' then
            CNT := (CNT + 1);
            CNT := CNT mod to_integer(CNT_MAX);
            CNT_OUT <= std_logic_vector(CNT);
        end if;         
    end if;
    
end process COUNTER_PROC;

end Behavioral;
