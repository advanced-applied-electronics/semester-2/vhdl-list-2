----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.11.2022 22:58:11
-- Design Name: 
-- Module Name: T3_8bit_counter - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity T3_8bit_counter is
    Port ( CLK : in STD_LOGIC;
           RST : in STD_LOGIC;
           CNT_VAL : out STD_LOGIC_VECTOR (7 downto 0));
end T3_8bit_counter;

architecture Behavioral of T3_8bit_counter is

begin
d_proc: process(CLK, RST)
variable CNT: unsigned(7 downto 0);
begin    

    if RST = '1' then
        CNT := (others => '0');
    
    elsif (rising_edge(CLK)) then
        CNT := CNT + 1;       
    
    end if;
    
    CNT_VAL <= std_logic_vector(CNT);
    
end process d_proc;


end Behavioral;
