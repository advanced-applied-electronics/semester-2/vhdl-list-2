----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01.12.2022 20:09:41
-- Design Name: 
-- Module Name: T5_freq_div_2 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.NUMERIC_STD_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity T5_freq_div_2 is
    Port ( CLK_IN : in STD_LOGIC;
           RST : in STD_LOGIC;
           CLK_OUT : out STD_LOGIC);
end T5_freq_div_2;

architecture Behavioral of T5_freq_div_2 is

begin
d_proc: process(CLK_IN, RST)
variable CNT: STD_LOGIC;
begin    

    if RST = '1' then
        CNT := '0';
        CLK_OUT <= '0';
    
    elsif (rising_edge(CLK_IN) and (CNT = '0')) then
        CNT := not CNT;
        CLK_OUT <= '1';
        
    elsif (rising_edge(CLK_IN) and (CNT = '1')) then
        CNT := not CNT;     
        CLK_OUT <= '0';
        
    end if;
    
end process d_proc;


end Behavioral;
