----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/01/2022 10:05:07 AM
-- Design Name: 
-- Module Name: T4_8bit_cnt_switch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity T4_8bit_cnt_switch is
    Port ( CLK : in STD_LOGIC;
           RST : in STD_LOGIC;
           SWITCH : in STD_LOGIC;
           D_OUT : out STD_LOGIC_VECTOR (7 downto 0));
end T4_8bit_cnt_switch;

architecture Behavioral of T4_8bit_cnt_switch is

begin
d_proc: process(CLK, RST, SWITCH)
variable CNT8: unsigned(7 downto 0);
variable CNT7: unsigned(6 downto 0);
variable MAX_VAL: unsigned(7 downto 0) := (others => '0');

begin
    if SWITCH'Event then
        CNT8 := (others => '0');
        CNT7 := (others => '0');
        D_OUT <= (others => '0');    
    end if;
    
    if RST = '1' then
        CNT8 := (others => '0');
        CNT7 := (others => '0');
        D_OUT <= (others => '0');
    elsif SWITCH='1' then    
        if (rising_edge(CLK)) then
            CNT8 := CNT8 + 1;
            end if;
        D_OUT <= std_logic_vector(CNT8);                   
    else
        if (rising_edge(CLK)) then
            CNT7 := CNT7 + 1;
            end if;
        D_OUT <= std_logic_vector('0'&CNT7);    
    end if;
        
end process d_proc;


end Behavioral;
