----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 30.11.2022 22:36:00
-- Design Name: 
-- Module Name: T2_D_latch - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity T2_D_latch is
    Port ( D : in STD_LOGIC;
           EN : in STD_LOGIC;
           Q : out STD_LOGIC);
end T2_D_latch;

architecture Behavioral of T2_D_latch is
signal tmp: std_logic;
begin
--d_proc: process(D, EN)
--begin    

--    if EN = '1' then
--        Q <=D;
--    end if;
--end process d_proc;        
    tmp <= D  when EN='1' else tmp;
    Q <= tmp;     

end Behavioral;
