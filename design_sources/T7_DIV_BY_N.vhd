----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 12/15/2022 10:46:46 AM
-- Design Name: 
-- Module Name: T7_DIV_BY_N - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity T7_DIV_BY_N is
    Port ( CLK : in STD_LOGIC;
           RST : in STD_LOGIC;
           S : in STD_LOGIC;
           MODULO : in STD_LOGIC_VECTOR (4 downto 0);
           CLK_OUT : out STD_LOGIC);
end T7_DIV_BY_N;

architecture Behavioral of T7_DIV_BY_N is
signal CLK_TMP: STD_LOGIC;
begin

COUNTER_PROC: process(CLK, RST, S)
variable CNT_MAX: unsigned (4 downto 0);
variable CNT: unsigned (4 downto 0);
begin  
    if RST = '1' then
        CNT_MAX := to_unsigned(1, CNT_MAX'length);
        CNT := (others =>'0');
        CLK_TMP <= 'Z';
    end if;                
        

    if rising_edge(CLK) and RST = '0' then
        if S = '1' then
            CNT_MAX := unsigned(MODULO);
            CNT := (others =>'0');
            CLK_TMP <= '0';
        elsif CNT_MAX = 0 or CNT_MAX = 1 then
            CLK_TMP <= CLK;
        elsif (CNT = 0) then
            CLK_TMP <= '1';
        elsif (CNT >= CNT_MAX) then
            CLK_TMP <= '0';
            CNT := to_unsigned(0, CNT_MAX'length);
        elsif (CNT >= CNT_MAX/2) then 
            CLK_TMP <= '0';                        
        end if;
        
        CNT := CNT + 1;     
    end if;
    
    CNT := CNT mod to_integer(CNT_MAX);
end process COUNTER_PROC;

CLK_OUT <= CLK when MODULO = "00000" else
           CLK when MODULO = "00001" else
           CLK_TMP;

end Behavioral;
