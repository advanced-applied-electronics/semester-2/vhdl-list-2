# VHDL List 2

## Requirements for opening project

- Vivado 2021.2

- Installed tcl scripts for git integration: https://github.com/barbedo/vivado-git

## How to open project using .tcl script

When opening the project after cloning it, do it by using `Tools -> Run Tcl Script...` and selecting the `vhdl-list-2.tcl`.

## How to open project if .tcl does not work

Create new project and just add files in project tree:

`RMB: Design Sources -> Add Sources -> Add Or Create Design Sources -> Add Directories -> choose /desing_sources/ directory.`

and:

`RMB: Simulation Sources -> Add Sources -> Add Or Create Simulation Sources -> Add Directories -> choose /test_benches/ directory.`

## How to commit changes

Follow instruction at https://github.com/barbedo/vivado-git

## Tasks & solutions

Click photo bellow to get the PDF.

[![Tasks](/doc/Tasks.png)](/doc/LISTA_2___FPGA_ENG.pdf)

### Task 1

![Task1](/doc/Task1.png)

![Task1 solution](/doc/Task1_Plot.png)

### Task 2

![Task2](/doc/Task2.png)

![Task2 solution](/doc/Task2_Plot.png)

### Task 3

![Task3](/doc/Task3.png)

![Task3 solution](/doc/Task3_Plot_A.png)

![Task3 solution](/doc/Task3_Plot_B.png)

![Task3 solution](/doc/Task3_Plot_C.png)

### Task 4

![Task4](/doc/Task4.png)

![Task4 solution](/doc/Task4_Plot_A.png)

![Task4 solution](/doc/Task4_Plot_B.png)

![Task4 solution](/doc/Task4_Plot_C.png)

### Task 5

![Task5](/doc/Task5.png)

![Task5 solution](/doc/Task5_Plot.png)

### Task 6

![Task6](/doc/Task6.png)

![Task6 solution](/doc/Task6_Plot1.png)

![Task6 solution](/doc/Task6_Plot2.png)

![Task6 solution](/doc/Task6_Plot3.png)

![Task6 solution](/doc/Task6_Plot4.png)

### Task 7

![Task7](/doc/Task7.png)

![Task7 solution](/doc/Task7_Plot1.png)

![Task7 solution](/doc/Task7_Plot2.png)

### Additional task

![Additional task](/doc/Additional_task.png)

## Author of solution

[**Piotr Zieliński**](https://gitlab.com/piter_t_ziel)

## Contribution

https://vhdl.lapinoo.net/testbench/