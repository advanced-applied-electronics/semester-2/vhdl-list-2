library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;

entity tb_T7_DIV_BY_N is
end tb_T7_DIV_BY_N;

architecture tb of tb_T7_DIV_BY_N is

    component T7_DIV_BY_N
        port (CLK     : in std_logic;
              RST     : in std_logic;
              S       : in std_logic;
              MODULO  : in std_logic_vector (4 downto 0);
              CLK_OUT : out std_logic);
    end component;

    signal CLK     : std_logic;
    signal RST     : std_logic;
    signal S       : std_logic;
    signal MODULO  : std_logic_vector (4 downto 0);
    signal CLK_OUT : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : T7_DIV_BY_N
    port map (CLK     => CLK,
              RST     => RST,
              S       => S,
              MODULO  => MODULO,
              CLK_OUT => CLK_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        S <= '0';
        MODULO <= (others => '0');

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 10ns;
        MODULO <= std_logic_vector(to_unsigned(1, MODULO'length));
        wait for 20 ns;
        S <= '1';
        wait for 10 ns;
        S <= '0';
        
        wait for 100ns;
        MODULO <= std_logic_vector(to_unsigned(4, MODULO'length));
        wait for 20 ns;
        S <= '1';
        wait for 10 ns;
        S <= '0';
        
        wait for 100ns;
        MODULO <= std_logic_vector(to_unsigned(3, MODULO'length));
        wait for 20 ns;
        S <= '1';
        wait for 10 ns;
        S <= '0';
        

        -- EDIT Add stimuli here  

        wait for 400 * TbPeriod;
        wait for 100 * TbPeriod;
        
        wait for 600 * TbPeriod;        

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;