-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 30.11.2022 21:42:42 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_T2_D_latch is
end tb_T2_D_latch;

architecture tb of tb_T2_D_latch is

    component T2_D_latch
        port (D  : in std_logic;
              EN : in std_logic;
              Q  : out std_logic);
    end component;

    signal D  : std_logic;
    signal EN : std_logic;
    signal Q  : std_logic;

begin

    dut : T2_D_latch
    port map (D  => D,
              EN => EN,
              Q  => Q);

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        D <= '0';
        EN <= '0';

        -- EDIT Add stimuli here
        wait for 10ns;        
        EN <= '1';
        wait for 10ns;
        D <= '1';
        wait for 10ns;
        EN <= '0';
        wait for 30ns;
        D <= '0';
        wait for 30ns;
        EN <= '1';
        wait;
    end process;

end tb;