-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 24.11.2022 09:47:56 UTC

library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.ALL;

entity tb_T1_32bit_register is
end tb_T1_32bit_register;

architecture tb of tb_T1_32bit_register is

    component T1_32bit_register
        port (WRITE_ENABLE : in std_logic;
              CLEAR        : in std_logic;
              CLK          : in std_logic;
              D_IN         : in std_logic_vector (31 downto 0);
              D_OUT        : out std_logic_vector (31 downto 0));
    end component;

    signal WRITE_ENABLE : std_logic;
    signal CLEAR        : std_logic;
    signal CLK          : std_logic;
    signal D_IN         : std_logic_vector (31 downto 0);
    signal D_OUT        : std_logic_vector (31 downto 0);

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : T1_32bit_register
    port map (WRITE_ENABLE => WRITE_ENABLE,
              CLEAR        => CLEAR,
              CLK          => CLK,
              D_IN         => D_IN,
              D_OUT        => D_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        WRITE_ENABLE <= '0';
        D_IN <= (others => '0');
        --D_OUT <= (others => '0');

        -- Reset generation
        -- EDIT: Check that CLEAR is really your reset signal
        CLEAR <= '1';
        wait for 100 ns;
        CLEAR <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        D_IN <= (others => '1');
        wait for 100 ns;
        WRITE_ENABLE <= '1';
        wait for 100 ns;
        D_IN <= X"F0F0F0F0";
        --D_IN <= std_logic_vector(to_unsigned(16#F0F0F0F0#, 32));
        wait for 42 ns;
        CLEAR <= '1';
        wait for 50 ns;
        WRITE_ENABLE <= '0';        
        wait for 50 ns;
        CLEAR <= '0';
        wait for 100 ns;
        --D_IN <= std_logic_vector(to_unsigned(16#0F0F0F0F#, 32));
        D_IN <= X"0F0F0F0F";
        wait for 100 ns;
        CLEAR <= '1';   
        
        wait for 100 * TbPeriod;
        

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;