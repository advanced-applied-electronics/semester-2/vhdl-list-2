-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 30.11.2022 21:59:36 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_T3_8bit_counter is
end tb_T3_8bit_counter;

architecture tb of tb_T3_8bit_counter is

    component T3_8bit_counter
        port (CLK     : in std_logic;
              RST     : in std_logic;
              CNT_VAL : out std_logic_vector (7 downto 0));
    end component;

    signal CLK     : std_logic;
    signal RST     : std_logic;
    signal CNT_VAL : std_logic_vector (7 downto 0);

    constant TbPeriod : time := 1 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : T3_8bit_counter
    port map (CLK     => CLK,
              RST     => RST,
              CNT_VAL => CNT_VAL);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;
        
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;
        
        wait for 1000 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;