-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 1.12.2022 19:27:11 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_T5_freq_div_2 is
end tb_T5_freq_div_2;

architecture tb of tb_T5_freq_div_2 is

    component T5_freq_div_2
        port (CLK_IN  : in std_logic;
              RST     : in std_logic;
              CLK_OUT : out std_logic);
    end component;

    signal CLK_IN  : std_logic;
    signal RST     : std_logic;
    signal CLK_OUT : std_logic;

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : T5_freq_div_2
    port map (CLK_IN  => CLK_IN,
              RST     => RST,
              CLK_OUT => CLK_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK_IN is really your main clock signal
    CLK_IN <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 100 * TbPeriod;

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;