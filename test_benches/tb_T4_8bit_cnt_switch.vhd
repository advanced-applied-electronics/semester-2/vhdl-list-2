-- Testbench automatically generated online
-- at https://vhdl.lapinoo.net
-- Generation date : 1.12.2022 09:19:21 UTC

library ieee;
use ieee.std_logic_1164.all;

entity tb_T4_8bit_cnt_switch is
end tb_T4_8bit_cnt_switch;

architecture tb of tb_T4_8bit_cnt_switch is

    component T4_8bit_cnt_switch
        port (CLK    : in std_logic;
              RST    : in std_logic;
              SWITCH : in std_logic;
              D_OUT  : out std_logic_vector (7 downto 0));
    end component;

    signal CLK    : std_logic;
    signal RST    : std_logic;
    signal SWITCH : std_logic;
    signal D_OUT  : std_logic_vector (7 downto 0);

    constant TbPeriod : time := 2 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : T4_8bit_cnt_switch
    port map (CLK    => CLK,
              RST    => RST,
              SWITCH => SWITCH,
              D_OUT  => D_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        SWITCH <= '0';

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;

        -- EDIT Add stimuli here
        wait for 600 * TbPeriod;
        
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 100 ns;
        wait for 400 * TbPeriod;
        SWITCH <= '1';
        wait for 400 * TbPeriod;
        

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;