library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

entity tb_T6_modulo_directional_counter is
end tb_T6_modulo_directional_counter;

architecture tb of tb_T6_modulo_directional_counter is

    component T6_modulo_directional_counter
        port (CLK     : in std_logic;
              RST     : in std_logic;
              DIR     : in std_logic;
              S       : in std_logic;
              MODULO  : in std_logic_vector (5 downto 0);
              CNT_OUT : out std_logic_vector (5 downto 0));
    end component;

    signal CLK     : std_logic;
    signal RST     : std_logic;
    signal DIR     : std_logic;
    signal S       : std_logic;
    signal MODULO  : std_logic_vector (5 downto 0);
    signal CNT_OUT : std_logic_vector (5 downto 0);

    constant TbPeriod : time := 10 ns; -- EDIT Put right period here
    signal TbClock : std_logic := '0';
    signal TbSimEnded : std_logic := '0';

begin

    dut : T6_modulo_directional_counter
    port map (CLK     => CLK,
              RST     => RST,
              DIR     => DIR,
              S       => S,
              MODULO  => MODULO,
              CNT_OUT => CNT_OUT);

    -- Clock generation
    TbClock <= not TbClock after TbPeriod/2 when TbSimEnded /= '1' else '0';

    -- EDIT: Check that CLK is really your main clock signal
    CLK <= TbClock;

    stimuli : process
    begin
        -- EDIT Adapt initialization as needed
        DIR <= '0';
        S <= '0';
        MODULO <= std_logic_vector(to_unsigned(2, MODULO'length));

        -- Reset generation
        -- EDIT: Check that RST is really your reset signal
        RST <= '1';
        wait for 100 ns;
        RST <= '0';
        wait for 10ns;
        MODULO <= std_logic_vector(to_unsigned(24, MODULO'length));
        wait for 100 ns;
        S <= '1';
        wait for 100 ns;
        S <= '0';
        

        -- EDIT Add stimuli here  
        DIR <= '0';
        wait for 400 * TbPeriod;
        DIR <= '1';
        wait for 400 * TbPeriod;
        wait for 100 * TbPeriod;
        
        wait for 600 * TbPeriod;        

        -- Stop the clock and hence terminate the simulation
        TbSimEnded <= '1';
        wait;
    end process;

end tb;